package com.example.widgetexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
RecyclerView rv;
RecyclerAdapter adapter;
String[] arr={"one","two","three"};
int[] imageArr={R.drawable.image1,R.drawable.image2,R.drawable.image1};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv=findViewById(R.id.rec_view);
        adapter=new RecyclerAdapter(arr,imageArr);
       // LinearLayoutManager manager=new LinearLayoutManager(this);
        GridLayoutManager manager=new GridLayoutManager(this,3);
        rv.setLayoutManager(manager);
        rv.setAdapter(adapter);
    }
}
